Changes
=======

Note: the versioning and dates are automatically filled in by the buildout fuillrelease script


1.0.1 (unreleased)
------------------

- Started with the empty framework

- Pinned iamreuasable app to version 0.0.9

- Pinned to Django 1.11

- Removed django-configurations package