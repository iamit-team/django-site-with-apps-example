Django example site with apps
==========================================

Apps build in in 2 different ways.

  * Embedded apps for the modules in this project (only used in this project)
  * Reusable apps: a seperated repo, using buildout extension to get it via git and build it



Default project docs
------

  * [change list](CHANGES.rst)
  * [TODO list](TODO.md)  
  * [Credits](CREDITS.md)


to build this project, see the site: [Buildout explain on IAmIT (http://iamit.nl/content/django-buildout-new-project/)](http://iamit.nl/content/django-buildout-new-project/)

or basically: clone this project.<br>
<br>
Install buildout globally, for python3:<br>
pip3 install zc.buildout<br>
<br>
cd in to the folder where the buildout.cfg file is, and:<br>
<br>
buildout<br>
bin/django migrate<br>
bin/django createsuperuser<br>
bin/django runserver<br>
<br>
That should start the project in your bowser on your localhost: [http://127.0.0.1:8000](http://127.0.0.1:8000)

In the repo of the example project belonging to the buildout explanation, there is a m,ore simple example (without the apps) of how to use buildout, and also better documentaion in the documentation folder there.<br>
i will add some extra docs for mac users (found out by one of my new employees/interns)<br>
<br>
see [https://bitbucket.org/iamit-team/django-buildout-example](https://bitbucket.org/iamit-team/django-buildout-example)


