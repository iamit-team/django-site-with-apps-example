from django.views.generic import TemplateView


class HomeView(TemplateView):
    template_name = "example_module/home.html"
