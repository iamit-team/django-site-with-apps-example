from .base import *
import os

os.environ.setdefault('DJANGO_CONFIGURATION', 'Development')

DEBUG = True
ALLOWED_HOSTS = ['127.0.0.1', ]


# for development we use sqllite, choose other db server for production
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BUILDOUT_DIR, 'var', 'sqlite', 'db_dev.sqlite3')
    }
}