import logging
from django.views.generic import TemplateView

logger = logging.getLogger(__name__)

class HomeView(TemplateView):
    template_name = "django_tutorial/home.html"

class SecondView(TemplateView):
    template_name = "django_tutorial/second_page.html"

class AnotherView(TemplateView):
    template_name = "django_tutorial/page_with_extra_context.html"

    def get_context_data(self, **kwargs):
        context = super(AnotherView, self).get_context_data(**kwargs)
        # Here you can add extra stuff to the context, we use this example:
        context['extra_something'] = 'Show how to use the context'
        return context
