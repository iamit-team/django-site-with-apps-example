# Note:  in you gunicorn service config, use this wsgi file

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "django_tutorials.settings.production")

#Should use HTTPS!
#os.environ['HTTPS'] = "on"

application = get_wsgi_application()
