[Back to index](index.md)

Install with buildout
=====================


Install pip (or pip3 for python3)

Than install buildout globally

pip install buildout

or

pip3 install buildout


Clone this repo in a project folder

cd in to the cloned folder (we call this the buildout folder, where buildout.cfg)


Than run:

    buildout bootstrap
    
    bin/buildout
    
    bin/django migrate
    
    bin/django createsuperuser
    
    bin/django runserver
   
Note: above will installl locally, and uses the Development environment (see: [Environments](environment.md))

to install on the server:

    bin/buildout -c production.cfg
    
    