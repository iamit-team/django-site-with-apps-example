from setuptools import setup

version = '1.0.0.dev0'

long_description = '\n\n'.join([
    open('README.md').read(),
    open('TODO.md').read(),
    open('CREDITS.md').read(),
    open('CHANGES.md').read(),
    ])

install_requires = [
    'Django',
    'setuptools',
    'pkginfo',
    'django-compressor',
    'django-filter',
    'gunicorn', # the webserver in production
    'django-compressor',
    'iamreusable',
    'gunicorn',
    'pytest',
    'pytest-django',
    'pytest-factoryboy',
],


setup(name='django-tutorial',
      version=version,
      url='',
      description="TODO",
      long_description=long_description,
      # Get strings from http://www.python.org/pypi?%3Aaction=list_classifiers
      classifiers=['Programming Language :: Python',
                   'Framework :: Django',
                   ],
      keywords=[],
      author='TODO',
      author_email='michel@iamit.nl',
      packages=['django_tutorial'],
      include_package_data=True,
      zip_safe=False,
      install_requires=install_requires,
      entry_points={
          'console_scripts': [
          ]},
      )
