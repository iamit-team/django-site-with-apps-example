from apps.example_module.utils import some_function



class TestExampleModuleClass:

    # example of testing the things in the app

    def test_one(self):
        tmp='Michel'
        tmp2='van Leeuwen'
        expected = 'Michel van Leeuwen'
        assert some_function(tmp,tmp2) == expected
