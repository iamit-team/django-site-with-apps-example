
#note: normally the function you want to test is not in the test classes it selve of course
def increase(x):
    return x + 1


class TestClass:

    def test_one(self):
        x = "this"
        assert 'h' in x

    def test_two(self):
        x = "servus"
        assert x == "hello"

    def test_increase1(self):
        assert increase(3) == 4

    def test_increase2(self):
        assert increase(3) == 5


